#!/usr/bin/env python3

import os, sys, math


scalePxToMm = 3.543307

params = dict()
params['radius'] = 78
params['lineThickness'] = 0.5
params['lineStride'] = 1
params['angle'] = 20
params['outputFile'] = 'bahtinovMask.svg'


description = '''
This script creates a Bahtinov mask, as a .svg file.
The script works in arbitrary units, and the .svg can be
scaled as desired.

The script\'s parameters and their default values are:
--radius 78                     the radius of the created circular mask
--lineThickness 0.5             the thickness of each line
--lineStride 1                  the distance between the, e.g., left side of
                                    two successive lines. The empty space
                                    between two lines is 
                                    lineStride - lineThickness
--angle 20                      the line deflection angle, in degrees (deg)
--outputFile bahtinovMask.svg   the name of the output file
'''

def getArgValue(ii, argv, errorMessage):
    if ii < len(argv) - 1:
        return( argv[ii + 1] )
    else:
        print(errorMessage)
        sys.exit(1)
            

def processCmd(argv):
    if len(argv) > 1 and argv[1] in ['-h', '--help']:
        print(description)
        sys.exit(0)

    for ii, arg in enumerate(argv):
        if arg.startswith('--'):
            param = arg.lstrip('-')
            if param not in params:
                print(description)
                sys.exit(1)
            params[ param ] = getArgValue(ii, argv, description)


processCmd(sys.argv)


radius = float(params['radius'])
lineThickness = float(params['lineThickness'])
lineStride = float(params['lineStride'])
angle = float(params['angle'])
outputFile = params['outputFile']



svg = ['<svg height="', '" width="', '">\n\
<clipPath id="clip1">\n  <circle cx="' + str(radius) + '" cy="' + str(radius) + '" r="' + str(radius) +'"/>\n</clipPath>\n\
<path clip-path="url(\'#clip1\')" style="fill:#', '" d="', '" />\n</svg>']


#heightMm widthMm path colour path
svgData = [ 2 * radius, 2 * radius, '000000', 'M150 0 L75 200 L225 200 Z' ]


def rotate(x, y, angle):
    xr = x * math.cos(angle) - y * math.sin(angle)
    yr = x * math.sin(angle) + y * math.cos(angle)
    return (xr, yr)

def restrictLineX(x0, y0, x1, y1, xr):
    '''restricts the first set of parameters (x0, y0) to xr -> (xr, yr), and returns yr'''
    # (x - x1) / (x0 - x1) = (y - y1) / (y0 - y1)
    # (xr - x1) / (x0 - x1) = (yr - y1) / (y0 - y1)
    # yr - y1 = (xr - x1) * (y0 - y1) / (x0 - x1)
    # yr = (xr - x1) * (y0 - y1) / (x0 - x1) + y1
    yr = (xr - x1) * (y0 - y1) / (x0 - x1) + y1
    return(yr)


def genPathPart(action, x, y):
    return(action + ' ' + str(x) + ',' + str(y) + ' ')

gpp = genPathPart



def drawFullLine(x, y, radius, angle, signum = +1):
    s = signum
    l  = gpp('M', x,            y)
    l += gpp('L', x + s * lineThickness, y)
    l += gpp('L', x + s * lineThickness, y + radius)

    xds = x + s * lineThickness / 2
    xdo = s * lineThickness / 2
    yds = y + radius
    ydo = 0
    #don't rotate, we want the empty space to be equal, not the lines
    #xdo, ydo = rotate(xdo, ydo, angle)
    xd = xds + xdo
    yd = yds + ydo
    l += gpp('L', xd, yd)
    
    xds = x + s * lineThickness / 2
    xdo = s * lineThickness / 2
    yds = y + radius
    ydo = radius * 2
    xdo, ydo = rotate(xdo, ydo, angle)
    xd = xds + xdo
    yd = yds + ydo
    l += gpp('L', xd, yd)
    
    xds = x + s * lineThickness / 2
    xdo = - s * lineThickness / 2
    yds = y + radius
    ydo = radius * 2
    xdo, ydo = rotate(xdo, ydo, angle)
    xd = xds + xdo
    yd = yds + ydo
    l += gpp('L', xd, yd)

    xds = x + s * lineThickness / 2
    xdo = - s * lineThickness / 2
    yds = y + radius
    ydo = 0
    #don't rotate, we want the empty space to be equal, not the lines
    #xdo, ydo = rotate(xdo, ydo, angle)
    xd = xds + xdo
    yd = yds + ydo
    l += gpp('L', xd, yd)
     
    l += gpp('L', x,            y + radius)
    l += gpp('L', x,            y)


    xr = radius
    
    xds = x + s * lineThickness / 2
    xdo = s * lineThickness / 2
    yds = y + radius
    ydo = 0
    #don't rotate, we want the empty space to be equal, not the lines
    #xdo, ydo = rotate(xdo, ydo, -angle)
    
    xd = xds + xdo
    yd = yds + ydo
    
    x0 = xd
    y0 = yd
    
    xds = x + s * lineThickness / 2
    xdo = s * lineThickness / 2
    yds = y + radius
    ydo = radius * 2
    xdo, ydo = rotate(xdo, ydo, -angle)
    xd = xds + xdo
    yd = yds + ydo

    x1 = xd
    y1 = yd

    xds = x + s * lineThickness / 2
    xdo = - s * lineThickness / 2
    yds = y + radius
    ydo = radius * 2
    xdo, ydo = rotate(xdo, ydo, -angle)
    xd = xds + xdo
    yd = yds + ydo

    x3 = xd
    y3 = yd
    
    xds = x + s * lineThickness / 2
    xdo = - s * lineThickness / 2
    yds = y + radius
    ydo = 0
    #don't rotate, we want the empty space to be equal, not the lines
    #xdo, ydo = rotate(xdo, ydo, -angle)
    xd = xds + xdo
    yd = yds + ydo

    x2 = xd
    y2 = yd
    #rectangle 0 1 3 2
    #line 0-1 will restrict 0
    #line 2-3 will restrict 2
    #line 1-3 will not be restricted (here, it will still be cropped)

    y0r = restrictLineX(x0, y0, x1, y1, xr)
    y2r = restrictLineX(x2, y2, x3, y3, xr)

    l += gpp('M', xr, y0r)
    l += gpp('L', x1, y1)
    l += gpp('L', x3, y3)
    l += gpp('L', xr, y2r)
    

    
    return(l)

path = ''
#verification only
y = 2
path += gpp('M', radius - lineThickness / 8, 0)
path += gpp('L', radius - lineThickness / 8, 2 * radius)
path += gpp('L', radius + lineThickness / 8, 2 * radius)
path += gpp('L', radius + lineThickness / 8, 0)
#reset here
path = ''
y = 0

angle = math.radians(angle)

x = radius + lineThickness / 2

while x >= 0:
    path += drawFullLine(x, y, radius, angle, -1)
    x -= lineStride

x = radius - lineThickness / 2 + lineStride

while x <= 2 * radius:
    path += drawFullLine(x, y, radius, -1 * angle, 1)
    x += lineStride
    
path += 'z'

svgData[-1] = path


with open(outputFile, 'w') as f:
    for ii in range( len(svgData) ):
        f.write(str( svg[ii] ) + str( svgData[ii] ))
    f.write(svg[-1])

print('Bahtinov mask written to', outputFile)
