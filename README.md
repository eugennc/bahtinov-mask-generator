# Bahtinov Mask Generator

A parametrised Python 3 script which generates a Bahtinov Mask for cameras as an .svg .
The script uses an arbitrary measurement unit, and scaling might be needed during e.g. laser engraving.
The shape is optimised for creating relatively small masks, for laser engraving, and for use in focusing camera lenses, not telescopes.

Launch the script without parameters to generate the default mask.
Use --help to see the full parameters.